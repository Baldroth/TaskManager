describe('Task Manager', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('should display the task manager title', () => {
    cy.contains('h1', 'Task Manager');
  });

  it('should allow adding a new task', () => {
    const taskName = 'Test new task';

    cy.get('input[placeholder="Add a new task"]').type(taskName);
    cy.contains('button', 'Add Task').click();

    
    cy.contains('li', taskName);
  });

  it('should allow completing a task', () => {
    const taskName = 'Task to complete';

    
    cy.get('input[placeholder="Add a new task"]').type(taskName);
    cy.contains('button', 'Add Task').click();

    
    cy.contains('li', taskName).find('input[type="checkbox"]').check();

    
    cy.contains('li', taskName).should('have.class', 'completed');
  });

  it('should allow uncompleting a task', () => {
    const taskName = 'Task to uncomplete';

    
    cy.get('input[placeholder="Add a new task"]').type(taskName);
    cy.contains('button', 'Add Task').click();

    
    cy.contains('li', taskName).find('input[type="checkbox"]').check();
    cy.contains('li', taskName).should('have.class', 'completed');
    cy.contains('li', taskName).find('input[type="checkbox"]').uncheck();

    
    cy.contains('li', taskName).should('not.have.class', 'completed');
  });
});
