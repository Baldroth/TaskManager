const { MongoMemoryServer } = require('mongodb-memory-server');
const mongoose = require('mongoose');

async function ConnectionToServer() {
  const mongoUrl = process.env.MONGO_URL || 'mongodb://mongodb:27017/taskmanager';
  try {
    await mongoose.connect(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
    console.log('Successfully connected to MongoDB');
    await mongoose.disconnect();
    return 'Connected';
  } catch (err) {
    console.error('Error connecting to MongoDB');
    await mongoose.disconnect();
    return 'Failed to connect';
  }
}

describe('MongoDB Connection Test', () => {
  let mongoServer;

  beforeAll(async () => {
    mongoServer = await MongoMemoryServer.create({
      binary: {
        version: '7.0.3'
      }
    });
    process.env.MONGO_URL = mongoServer.getUri();
  });

  afterAll(async () => {
    await mongoServer.stop();
  });

  it('should connect to MongoDB successfully', async () => {
    const result = await ConnectionToServer();
    expect(result).toBe('Connected');
  });

  it('should fail to connect to MongoDB with an invalid URL', async () => {
    process.env.MONGO_URL = 'mongodb://DOES_NOT_WORK';
    const result = await ConnectionToServer();
    expect(result).toBe('Failed to connect');
  });
});
